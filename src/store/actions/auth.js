import * as actionTypes from './actionTypes'
import axios from 'axios'

// Pakai Axios biasa karena untuk auth di firebase itu urlnya beda, bisa juga sih buat file axios baru dengan instance yang baru
// key=[] < ambil di, URL nya ambil yang signup / bisa diliat selengkapnya di https://firebase.google.com/docs/reference/rest/auth
// returnSecureToken itu default dari firebase
// Di Firebase validasi backend untuk password minimal 6 karakter, automatis tervalidasi
// SignIn : Login | SignUp : Register
// err.response.data.error : response.data.error nya itu default dari firebase / langsung jadi
// Sync : Ada perubahan state di reducer | Async : tidak ada perubahan, tapi dia menjalankan Sync code nya dengan dispatch / middlewarenya thunk
// checkAuthTimeout : Logout automatis mulai dari 3600 (1 jam / default dari firebase) | expirationTime * 1000 : karena di setTimeout 3600 itu 3,6 Milisecond, jadi biar dia sync dengan second maka di kalikan 1000
export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = (token, userId) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        idToken: token,
        userId: userId
    }
}

export const authFail = (err) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: err
    }
}

export const logout = () => {
    localStorage.removeItem('token')
    localStorage.removeItem('expirationDate')
    localStorage.removeItem('userId')
    return {
        type: actionTypes.AUTH_LOGOUT
    }
}

export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
           dispatch(logout())
        }, expirationTime * 1000);       
    }
}

export const auth = (email, password, isSignUp) => {
    return dispatch => {
        dispatch(authStart());
        const authData = {
            email: email,
            password: password,
            returnSecureToken: true
        }

        let url = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDJVRaGXPD2X3xbz3o7RwyERyLpntPjXG4';
        if (!isSignUp) {
            url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDJVRaGXPD2X3xbz3o7RwyERyLpntPjXG4'
        }

        axios.post(url, authData)
            .then(res => {
                const expirationDate = new Date(new Date().getTime() + res.data.expiresIn * 1000)
                localStorage.setItem('token', res.data.idToken)
                localStorage.setItem('expirationDate', expirationDate)
                localStorage.setItem('userId', res.data.localId)
                dispatch(authSuccess(res.data.idToken, res.data.localId))
                dispatch(checkAuthTimeout(res.data.expiresIn))
            })
            .catch(err => {
                console.log(err)
                dispatch(authFail(err.response.data.error))
            })
    }
}

export const setAuthRedirectPath = (path) => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path: path
    }
}

// Jika token masih ada maka tidak logout => Jika Expired >= Waktu sekarang maka masih sukses, jika tidak ya logout
export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token')
        if (!token) {
            dispatch(logout())
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'))
            const expirationDateInTime = (expirationDate.getTime() - new Date().getTime()) / 1000
            if (expirationDate >= new Date()) {
                const userId = localStorage.getItem('userId')
                dispatch(authSuccess(token, userId))
                dispatch(checkAuthTimeout(expirationDateInTime))   
            } else {
                dispatch(logout())
            }
        }
    }
}