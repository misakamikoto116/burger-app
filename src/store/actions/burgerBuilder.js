import * as actionTypes from './actionTypes'
import axios from '../../axios'

export const addIng = (name) => {
    return {
        type: actionTypes.ADD_ING,
        ingredientName: name
    }
}

export const rmvIng = (name) => {
    return {
        type: actionTypes.RMV_ING,
        ingredientName: name
    }
}

export const setIngredients = (ingredients) => {
    return {
        type: actionTypes.SET_ING,
        ingredients: ingredients
    }
}

export const fetchIngredientsFailed = () => {
    return {
        type: actionTypes.FETCH_INGREDIENTS_FAILED
    }
}

export const initIngredients = () => {
    return dispatch => {
        axios.get('ingredients.json')
              .then(res => {
                  dispatch(setIngredients(res.data))
              })
              .catch(err => {
                  console.log(err)
                  dispatch(fetchIngredientsFailed())       
              });
    }
}