import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../../shared/utility'

// ...state.ingredients,
// [action.ingredientName] : state.ingredients[ingredientName] - 1
// Artinya Habisa di copy, lalu di override dimana state.ingredients[key] jadi -1 valuenya
const initialState = {
    ingredients: null,
    totalPrice: 2,
    error: false,
    building: false,
}

// Global Const
const INGREDIENT_PRICES = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.6
}

const addIngredient = (state, action) => {
    const updatedIngredient = { [action.ingredientName] : state.ingredients[action.ingredientName] + 1 };
    const updatedIngredients = updateObject(state.ingredients, updatedIngredient);
    const updatedState = {
        ingredients: updatedIngredients,
        totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingredientName],
        building: true
    }
    
    return updateObject(state, updatedState);
}

const removeIngredient = (state, action) => {
    const updatedIng = { [action.ingredientName] : state.ingredients[action.ingredientName] - 1}
    const updatedIngs = updateObject(state.ingredients, updatedIng)
    const updateState = {
        ingredients: updatedIngs,
        totalPrice: state.totalPrice - INGREDIENT_PRICES[action.ingredientName],
        building: true
    }
    return updateObject(state, updateState)
}

const setIngredient = (state, action) => {
    return updateObject(state, {
        ingredients: {
            salad: action.ingredients.salad,
            bacon: action.ingredients.bacon,
            cheese: action.ingredients.cheese,
            meat: action.ingredients.meat
        },
        totalPrice: 4,
        error: false,
        building: false
    })
}

const fetchIngredientsFailed = (state, action) => {
    return updateObject(state, {error: true})
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_ING: return addIngredient(state, action)
        case actionTypes.RMV_ING: return removeIngredient(state, action)
        case actionTypes.SET_ING: return setIngredient(state, action)
        case actionTypes.FETCH_INGREDIENTS_FAILED: return fetchIngredientsFailed(state, action)
        default: return state;
    }
}

export default reducer