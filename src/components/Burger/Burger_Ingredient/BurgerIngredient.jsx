import React, {Component} from 'react'
import Class from './BurgerIngredient.module.css'
import PropTypes from 'prop-types'

class BurgerIngredient extends Component {
    render() {
        let ingredient = null
    
        switch (this.props.type) {
            case ('bread-bottom'):
                ingredient = <div className={Class.BreadBottom}></div>
                break;
    
            case ('bread-top'):
                ingredient = (
                    <div className={Class.BreadTop}>
                        <div className={Class.Seeds1}></div>
                        <div className={Class.Seeds2}></div>
                    </div>
                )
                break;
            
            case ('meat'):
                ingredient = <div className={Class.Meat}></div>
                break;
    
            case ('cheese'):
                ingredient = <div className={Class.Cheese}></div>
                break;
    
            case ('bacon'):
                ingredient = <div className={Class.Bacon}></div>
                break;
                
            case ('salad'):
                ingredient = <div className={Class.Salad}></div>
                break;
    
            default:
                ingredient = null
                break;
        }
    
        return ingredient
    }
}

BurgerIngredient.propTypes = {
    type: PropTypes.string.isRequired
}

export default BurgerIngredient