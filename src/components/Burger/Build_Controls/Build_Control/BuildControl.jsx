import React from 'react'
import Class from './BuildControl.module.css'

const BuildControl = (props) => (
    <div className={Class.BuildControl}>
        <div className={Class.Label}>{props.label}</div>
        <button 
            className={Class.Less} 
            onClick={props.removed}
            disabled={props.disabled}>Less</button>
        <button 
            className={Class.More} 
            onClick={props.added}>More</button>
    </div>
);

export default BuildControl;