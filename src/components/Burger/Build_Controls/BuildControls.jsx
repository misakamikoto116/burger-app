import React from "react";
import Class from './BuildControls.module.css'
import BuildControl from './Build_Control/BuildControl'

// Testing
const controls = [
    { label: 'Salad', type: 'salad'},
    { label: 'Bacon', type: 'bacon'},
    { label: 'Cheese', type: 'cheese'},
    { label: 'Meat', type: 'meat'}
]

// => () langsung return kalau => {} harus kasih return Lagi
// Disabled : Cari array Berdasarkan Ctrl.type.. jika <= maka true, jika >= maka false / Sudah di set di Burger Builder.. Dia filter yang <=, tidak ada type yang jumlahnya > 0 maka dia false
// ToFixed : Membulatkan sesuai 2 angka dibelakang Koma / Di Statenya gk di Dibulatkan
const BuildControls = (props) => (
    <div className={Class.BuildControls}>
        <p>Current Price : <strong>{props.price.toFixed(2)}</strong></p>
        { controls.map(ctrl => (
            <BuildControl 
                key={ctrl.label} 
                label={ctrl.label}
                added={() => props.ingredientAdded(ctrl.type)}
                removed={() => props.ingredientRemoved(ctrl.type) }
                disabled={props.disabled[ctrl.type]} />
        ))}
        <button 
            className={Class.OrderButton}
            disabled={!props.purchasable}
            onClick={props.ordered}>{props.isAuth ? 'order now' : 'sign up to order'}</button>
    </div>
)

export default BuildControls