import React from 'react';
import Class from './Burger.module.css'
import { withRouter } from 'react-router-dom'
import BurgerIngredient from './Burger_Ingredient/BurgerIngredient'

// pake Div Karena butuh Wrapper / Pembungkus
// transformedIngredients :
// Convert Dari Object Key Array Dan memisahkan Value berdasarkan jumlah isi burgernya
// Map yang pertama hanya mengambil Key nya aja misal Meat atau Bacon
// Map yang kedua Search By Key Lalu Map Jumlahnya.. misal jumlahny ada 3
// ada map _, i itu gk peduli valuenya, cukup ambil Indexnya aja buat Key
// di Reduce, bagian [] adalah semuanya dimulai dari [], Jika elemen nya ada maka arranyay diisi dengan el, inginya dia dijadikan 1 array, Agar bisa dihitung length nya
// Concat sama aja dengan ...
// WithRouter Digunakan untuk melakukan eject pada component tertentu, karena disini dia gk terset sebagai route, maka bisa di set secara manual dengan hoc withRouter ini

const Burger = (props) => {
    let transformedIngredients = Object.keys( props.ingredients )
        .map( igKey => {
            return [...Array( props.ingredients[igKey] )].map( ( _, i ) => {
                return <BurgerIngredient key={igKey + i} type={igKey} />;
            } );
        } )
        .reduce((arr, el) => {
            return arr.concat(el)
        }, []);
    if (transformedIngredients.length === 0) {
        transformedIngredients = <p>Please start adding ingredients!</p>;
    }

    return (
        <div className={Class.Burger}>
            <BurgerIngredient type="bread-top"/>
            {transformedIngredients}
            <BurgerIngredient type="bread-bottom"/>
        </div>
    );
}

export default withRouter(Burger)