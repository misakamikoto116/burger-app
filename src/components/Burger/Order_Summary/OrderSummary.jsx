import React, {Component} from 'react'
import Aux from '../../../hoc/Auxiliary/Auxiliary'
import Button from '../../UI/Button/Button'

// Gunakan Aux karena gk butuh Pembungkus / Wrapper
// pakai ShouldComponent Update, Karena Di Burger builder, Component ini diupdate / modal nya di show or hidden
// ini Gk harus Class sih, tapi cuma mau cek aja pakai componentDidupdate, kalau udh gk ngecek lagi bisa jadiin function aja biar lebih ringan lagi
class OrderSummary extends Component {
    render() {
        const ingredientSummary = Object.keys(this.props.ingredients)
            .map(igKey => {
                return (
                    <li key={igKey}> 
                        <span style={{textTransform: 'capitalize'}}>{igKey} :</span>
                        {this.props.ingredients[igKey]}
                    </li>
                )   
            })
    
        return (
            <Aux>
                <h3>Your Order</h3>
                <p>A Delicious Burger with the following ingredients :</p>
                <ul>
                    {ingredientSummary}
                </ul>
                <p><strong>Total Price: {this.props.price.toFixed( 2 )}</strong></p>
                <p>Continue to checkout?</p>
                <Button btnType="Danger" clicked={this.props.purchaseCanceled}>CANCEL</Button>
                <Button btnType="Success" clicked={this.props.purchaseContinued}>CONTINUE</Button>
            </Aux>
        )
    }
}

export default OrderSummary;