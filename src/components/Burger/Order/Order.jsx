import React from 'react'
import Class from './Order.module.css'
// Number.parseFloat(props.price.toFixed(2)) convert ke float + tampilkan 2 aja yg dibelakang koma 

const Order = (props) => {
    const ingredients = [];

    for (let IngredientsName in props.ingredients) {
        ingredients.push({
            name: IngredientsName,
            amount: props.ingredients[IngredientsName]
        })
    }

    const ingredientOutput = ingredients.map(ig => {
        return <span 
                    className={Class.Ingredient_item} 
                    key={ig.name}>
                    {ig.name} ({ig.amount})
                </span>
    })

    return (
        <div className={Class.Order}>
            <p>Ingredients: {ingredientOutput}</p>
            <p>Price: <strong>USD {Number.parseFloat(props.price.toFixed(2))}</strong></p>
        </div>
    )
}

export default Order