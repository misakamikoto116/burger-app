import React from 'react'
import Button from '../../../UI/Button/Button'
import Burger from '../../Burger'
import Class from './CheckoutSummary.module.css'

const CheckoutSummary = (props) => (
    <div className={Class.CheckoutSummary}>
        <h1>We Hope it tastes well !</h1>
        <div style={{width: '100%', margin: 'auto'}}>
            <Burger ingredients={props.ingredients} />
        </div>
        <Button 
            btnType="Danger"
            clicked={props.checkoutCancelled}>Cancel</Button>
        <Button 
            btnType="Success"
            clicked={props.checkoutContinued}>Continue</Button>
    </div>
)

export default CheckoutSummary