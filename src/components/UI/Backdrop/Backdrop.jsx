import React from 'react';
import Class from './Backdrop.module.css'

const Backdrop = (props) => (
    props.show ? <div className={Class.Backdrop} onClick={props.clicked}></div> : null 
)

export default Backdrop