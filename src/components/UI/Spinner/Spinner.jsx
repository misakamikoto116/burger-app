import React from 'react'
import Class from './Spinner.module.css'


const Spinner = () => (
    <div className={Class.Loader}>Loading...</div>
)

export default Spinner;