import React from 'react'
import Class from './Button.module.css'

// Type kaya Success Atau Danger itu di set dari Props, lalu Joinkan ntar jadinya "Button success"
const Button = (props) => (
    <button
        disabled={props.disabled}
        className={[Class.Button, Class[props.btnType]].join(' ')} 
        onClick={props.clicked}>{props.children}</button>
)

export default Button;