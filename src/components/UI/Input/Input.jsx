import React from 'react'
import Class from './Input.module.css'

// Jadi di input tuh ada {...props.elementConfig} buat nyamain aja sama JSX custom dan tag html nya

const Input = (props) => {
    let inputElement = null;
    let validationError = null;
    const inputClasses = [Class.InputElement];

    if (props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push(Class.Invalid)
        validationError = <p className={Class.ValidationError}>Please enter a valid {props.label}!</p>;
    }

    switch (props.elementType) {
        case ('input'):
            inputElement = <input 
                            className={inputClasses.join(' ')} 
                            {...props.elementConfig}
                            value={props.value}
                            onChange={props.changed} />
            break;

        case ('textarea'):
            inputElement = <textarea 
                            className={inputClasses.join(' ')} 
                            {...props.elementConfig}
                            value={props.value}
                            onChange={props.changed} />
            break;    
        
        case ('select'):
            inputElement = <select 
                            className={inputClasses.join(' ')} 
                            value={props.value}
                            onChange={props.changed}>
                                {props.elementConfig.options.map(option => (
                                    <option key={option.value} value={option.value}>
                                        {option.displayValue}
                                    </option>
                                ))}
                            </select>
            break;    
    
        default:
            inputElement = <input 
                            className={inputClasses.join(' ')} 
                            {...props.elementConfig}
                            value={props.value}
                            onChange={props.changed} />;
            break;
    }

    return (
        <div className={Class.Input}>
            <label className={Class.Label}>{props.label}</label>
            {inputElement}
            {validationError}
        </div>
    );
}

export default Input