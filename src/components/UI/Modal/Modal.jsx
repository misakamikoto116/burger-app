import React, {Component} from 'react'
import Class from './Modal.module.css'
import Aux from '../../../hoc/Auxiliary/Auxiliary'
import Backdrop from '../Backdrop/Backdrop'

// ShouldComponentUpdate : Component did update akan jalan ketika props.show yang sekarang beda dengan props yang baru diupdate dari BurgerBuilder (Entah itu buka modal atau tutup modal).. jadi semuanya diupdate ketika kita klik orderNow (Kurang lebih kaya Submit di PHP pencarian, jadi bukan update setiap kita ngetik, tapi setiap submits) 
// Jika disini ShouldComponentUpdate, maka di OrderSummary automatis ngikut sini updatenya.. karena dia children dari Modal ini
// tambahan : jika disini tidak diupdate maka di Ordersummary, pun tidak diupdate.. jadi semuanya akan diupdate ketika tombol ordernow di tekan
// Bisa juga pakai PureComponent (Tapi jika pakai ini maka Modalclosed Juga kena / Karena dia props function, karena itu disini kita gk pakai itu, karena yang kita check hanya show nya saja )
// jadi ini Aku kasih ||nextProps.children !== this.props.children Karena :
// di BugerBuilder Children Dari Modal Diganti dari OrderSummary Ataupun Spinner, karena itu kita perlu update juga jika children nya berubah, maksudnya jika props children berubah maka component ini bisa berubah (Show or Hide)
class Modal extends Component {
    shouldComponentUpdate(nextProps, nexState) {
        return nextProps.show !== this.props.show || nextProps.children !== this.props.children
    }

    render() {
        const modalVisible = {
            transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
            opacity: this.props.show ? '1' : '0'
        } 
    
        return (
            <Aux>
                <Backdrop show={this.props.show} clicked={this.props.modalClosed}/>
                <div className={Class.Modal} style={modalVisible}>
                    {this.props.children}
                </div>
            </Aux>
        )
    }
}

export default Modal;