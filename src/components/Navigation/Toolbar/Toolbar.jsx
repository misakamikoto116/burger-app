import React from 'react'
import Class from './Toolbar.module.css'
import Logo from '../../Logo/Logo'
import NavigationItems from '../NavigationItems/NavigationItems'
import DrawerToggle from '../../SideDrawer/DrawerToggle/DrawerToggle'

const Toolbar = (props) => (
    <header className={Class.Toolbar}>
        <DrawerToggle clicked={props.drawerToggleClicked}/>
        <div className={Class.Logo}>
            <Logo/>
        </div>
        <nav className={Class.dekstopOnly}>
            <NavigationItems isAuthenticated={props.isAuth}/>
        </nav>
    </header>
)

export default Toolbar;