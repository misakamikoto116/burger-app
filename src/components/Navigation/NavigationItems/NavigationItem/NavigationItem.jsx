import React from 'react'
import { NavLink } from 'react-router-dom'
import Class from './NavigationItem.module.css'

const NavigationItem = (props) => (
    <li className={Class.NavigationItem}>
        <NavLink
            activeClassName={Class.active} 
            exact={props.exact}
            to={props.link}> {props.children}
        </NavLink>
    </li>
)

export default NavigationItem