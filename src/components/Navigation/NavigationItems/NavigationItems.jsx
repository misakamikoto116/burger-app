import React from 'react'
import Class from './NavigationItems.module.css'
import NavigationItem from './NavigationItem/NavigationItem'

// active : Jika boolean dan dia true dia tidak perlu mengganakan active={true}
// Exact disini akan di lempar ke Navlink yang ada props exact nya, dimana dia memastikan class Active nya
const NavigationItems = (props) => (
    <ul className={Class.NavigationItems}>
        <NavigationItem link="/" active exact>Burger Builder</NavigationItem>
        {props.isAuthenticated && <NavigationItem link="/orders">Orders</NavigationItem>}
        {!props.isAuthenticated 
            ? <NavigationItem link="/auth">Authenticate</NavigationItem>
            : <NavigationItem link="/logout">Logout</NavigationItem>
        }
    </ul>
)

export default NavigationItems