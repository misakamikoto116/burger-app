import React from 'react'
import Class from './Logo.module.css'
import burgerLogo from '../../assets/images/logo.png'

const Logo = (props) => (
    <div className={Class.Logo} style={{height: props.height}}>
        <img src={burgerLogo} alt="MyBurger"/>
    </div>
)

export default Logo;