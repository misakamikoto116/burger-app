import React from 'react'
import Class from './DrawerToggle.module.css'

const DrawerToggle = (props) => (
    <div className={Class.DrawerToggle} onClick={props.clicked}>
        <div></div>
        <div></div>
        <div></div>
    </div>
)

export default DrawerToggle