import React from 'react'
import Logo from '../Logo/Logo'
import NavigationItems from '../Navigation/NavigationItems/NavigationItems'
import Class from './SideDrawer.module.css'
import Aux from '../../hoc/Auxiliary/Auxiliary'
import Backdrop from '../UI/Backdrop/Backdrop'

const SideDrawer = (props) => {
    let attachedClasses = [Class.SideDrawer, Class.Close]
    if (props.open) {
        attachedClasses = [Class.SideDrawer, Class.Open]
    }

    return (
        <Aux>
            <Backdrop show={props.open} clicked={props.closed}/>
            <div className={attachedClasses.join(' ')} onClick={props.closed}>
                <div className={Class.Logo}>
                    <Logo/>
                </div>
                <nav>
                    <NavigationItems isAuthenticated={props.isAuth}/>
                </nav>
            </div>
        </Aux>
    )
}

export default SideDrawer;