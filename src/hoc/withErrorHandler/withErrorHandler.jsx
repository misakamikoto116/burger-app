import React, {Component} from 'react'
import Aux from '../Auxiliary/Auxiliary'
import Modal from '../../components/UI/Modal/Modal'

// Jadi HOC ini gunanya untuk menampilkan error dalam Modal ketika ada response error dari axios
// 1. Component Did Mount : Bagian request.. jika dia ngerequest lagi / kirim request http lagi maka error nya jadikan null lagi,  dan bagian response.. jika ada error maka state error diubah jadi error response nya
// 2. Buat error Confirm handler ketika kita click backdrop maka errornya dijadikan null kembali
// 3. Bagian Modal show pake 2 attr show dan modalClosed, dimana itu akan ngaruh ke backdrop nya yang mana : jika props show tidak null maka backdrop muncul, jika null backdrop hilang / Modal nya juga, Modal closed adalah func untuk mengubah isi state error jadi null yang mana akan ngaruh juga ke attr show tadi
// 4. Kita membuat class dalam function, karena kita butuh 2 param maka gunakanlah function, tapi karena kita juga butuh lifecycle maka kasih aja class extends di return bawah, bisa juga pake react hooks
// 5. this.state.error.message : Karena tadi kita ubah state error jadi response error dari firebase.. nah di firebase ada .message nya
//6. WrappedComponent {...this.props}... Menggambungkan props kembali
// atur interceptornya pake construct karena kalo pake didmount itu diproses nya sesudah render, sementara kebutuhan kita errornya harus di handler sebelum di render
// ComponentWillUnount : Jadi jika banyak component yang pake nih hoc, gk perlu buat interceptor banyak, cukup bikin 1 trus hapus, trus bikin lagi, jadi ketika unmount maka interceptor di ejectm lalu setelah masuk ke component yang pake nih hoc, di didmount lagi (Di set lagi)
const WithErrorHandler = (WrappedComponent, axios) => {
    return  class extends Component {
        constructor(props) {
            super(props);
            this.reqInterceptors = axios.interceptors.request.use(req => {
                this.setState({error: null})
                return req
            })

            this.resInterceptors = axios.interceptors.response.use(null, err => {
                this.setState({error: err})
            })
        }

        state = {
            error: null
        }

        errorConfirmedHandler = () => {
            this.setState({error: null})
        }

        componentWillUnmount() {
            axios.interceptors.request.eject(this.reqInterceptors)
            axios.interceptors.response.eject(this.resInterceptors)
        }

        render() {
            return (
                <Aux>
                    <Modal show={this.state.error} modalClosed={this.errorConfirmedHandler}>
                        {this.state.error ? this.state.error.message : null}
                    </Modal>
                    <WrappedComponent {...this.props} />
                </Aux>
            )
        }
    }
}

export default WithErrorHandler