import React, {Component} from 'react';
import Layout from './hoc/Layout/Layout'
import { Route, Switch, withRouter, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import asyncComponent from './hoc/asyncComponent/asyncComponent'

import * as actions from './store/actions/index'
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder'
import Logout from './containers/Auth/Logout/Logout'

// Lazy Loading
const asyncCheckout = asyncComponent(() => {
  return import('./containers/Checkout/Checkout')
});

const asyncOrders = asyncComponent(() => {
  return import('./containers/Orders/Orders')
});

const asyncAuth = asyncComponent(() => {
  return import('./containers/Auth/Auth')
});

// Selain pake redirect bisa juga pake route path kosong, buat 404
class App extends Component {
  componentDidMount() {
    this.props.onTryAuthSignUp()
  }

  render(){
    let routes = (
      <Switch>
        <Route path="/" exact component={BurgerBuilder} />
        <Route path="/auth" component={asyncAuth} />
        <Redirect to="/"/>
      </Switch>
    )

    if (this.props.isAuth) {
      routes = (
        <Switch>
            <Route path="/" exact component={BurgerBuilder} />
            <Route path="/auth" component={asyncAuth} />
            <Route path="/orders" component={asyncOrders} />
            <Route path="/logout" component={Logout} />
            <Route path="/checkout" component={asyncCheckout} />
            <Redirect to="/"/>
          </Switch>        
      )
    }

    return (
      <div>
        <Layout>
          {routes}      
        </Layout>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    isAuth: state.auth.token !== null
  }
}

const mapDispathToProps = dispatch => {
  return {
    onTryAuthSignUp: () => dispatch( actions.authCheckState() )
  }
}

export default withRouter(connect(mapStateToProps, mapDispathToProps)(App));