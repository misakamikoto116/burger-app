import React, {Component} from 'react'
import { connect } from 'react-redux'
import Button from '../../../components/UI/Button/Button'
import Class from './ContactData.module.css'
import Spinner from '../../../components/UI/Spinner/Spinner'
import axios from '../../../axios'
import Input from '../../../components/UI/Input/Input'
import * as orderAction from '../../../store/actions/index'
import { updateObject } from '../../../shared/utility'
import { checkValidity } from '../../../shared/validation'
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler'

// State bercabang ini adalah akan yang dimasukkan ke props Input
//  {formElementsArray.map(formEl : map state yang sudah jadi array tadi
//  isValid = value.trim() !== '' && isValid   : jika value.trim() !== '' maka isValid jadi true, karena hasil dari trim() !== '' jika benar maka dia jadi true, jika tidak maka dia jadi false, (&& isValid) : jika value.trim() !== '' dia tidak ketemu maka isValid tadi jadi false anggapannya gini :
// isValid = value.trim() !== '' && isValid ? true : false

// Semua props ingredients dan totalPrice di set dari redux

class ContactData extends Component {
    state = {
        orderForm: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                touched: false
            },
            country: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Country'
                },
                label: 'Country',
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            street: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Street'
                },
                label: 'Street',
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            zipCode: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'ZIP Code'
                },
                label: 'Zip Code',
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    maxLength: 5,
                    isNumeric: true
                },
                valid: false,
                touched: false
            },
            email: {
                elementType: 'email',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Your Email'
                },
                label: 'Email',
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            deliveryMethod: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        {value: 'fastest', displayValue: 'fastest'},
                        {value: 'cheapest', displayValue: 'cheapest'}
                    ]
                },
                label: 'Delivery Method',
                value: 'fastest',
                validation: {},
                valid: true
            },
        },
        formIsValid: false,
    }

    orderHandler = (e) => {
        e.preventDefault();
        this.setState({loading: true})
        const formData = {};
        for (let formElementIdentifier in this.state.orderForm) {
            formData[formElementIdentifier] = this.state.orderForm[formElementIdentifier].value
        }

        const order = {
            ingredients: this.props.ingredients,
            price: this.props.totalPrice,
            orderData: formData,
            userId: this.props.userId
        }

        this.props.onOrderBurger(this.props.token, order)
    }

    inputChangedHandler = (e, identifier) => {
        const updatedFormElement = updateObject(this.state.orderForm[identifier], {
            value: e.target.value,
            valid: checkValidity(e.target.value, this.state.orderForm[identifier].validation),
            touched: true
        })

        const updatedOrderForm = updateObject(this.state.orderForm, {
            [identifier]: updatedFormElement
        })

        let formIsValid = true
        for (let inputIdentifier in updatedOrderForm) {
            formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid
        }

        this.setState({orderForm : updatedOrderForm, formIsValid: formIsValid})
    } 


    render() {
        const formElementsArray = [];

        for (let key in this.state.orderForm) {
            formElementsArray.push({
                id: key,
                config: this.state.orderForm[key]
            })
        }

        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formEl => (
                    <Input key={formEl.id}
                           elementType={formEl.config.elementType} 
                           elementConfig={formEl.config.elementConfig} 
                           value={formEl.config.value}
                           invalid={!formEl.config.valid}
                           shouldValidate={formEl.config.validation}
                           touched={formEl.config.touched}
                           label={formEl.config.label}
                           validation={formEl.config.validation}
                           changed={(event) => this.inputChangedHandler(event, formEl.id)}/>
                ))}
                <Button btnType="Success" disabled={!this.state.formIsValid}>Order</Button>
            </form>
        )

        if (this.props.loading) {
            form = <Spinner />
        }

        return (
            <div className={Class.ContactData}>
                <h4>Enter your Contact Data</h4>
                {form}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        loading: state.order.loading,
        token: state.auth.token,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onOrderBurger: (token, orderData) => dispatch( orderAction.purchaseBurger(token, orderData) )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(ContactData, axios))