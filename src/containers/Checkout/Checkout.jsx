import React, {Component} from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import Aux from '../../hoc/Auxiliary/Auxiliary'
import ContactData from './ContactData/ContactData'
import CheckoutSummary from '../../components/Burger/Order/CheckoutSummary/CheckoutSummary'

// path={this.props.match.path + '/contact-data'} : Path sekarang + /contact-data => checkout/contact-data
// Cara mengirim props melalui Route adalah dengan render
// Di Return Tanpa pake if state ingrediets bisa juga pake component Will mount / Constructor
// Jika tanpa parsing props untuk ngambil history route nya : gk perlu kasih param (props) di render, tapi harus pakai withRouter di Contact data, dibagian export nya
// <ContactData ingredients={this.state.ingredients} totalPrice={this.state.totalPrice}/> )}

// NOTE : yang dibawah itu lempar props aja, jadi gk perlu withRouter
// UPDATE : Gk perlu pake render lagi, karena di redux semua state jadi props jadi gk perlu lempar state lagi.. tapi nnti di component tersebut kita harus mengambil state nya di global state (kaya redux biasanya)

class Checkout extends Component {
    checkoutContinuedHandler = () => {
        this.props.history.replace('/checkout/contact-data');
    }

    checkoutCancelledHandler = () => {
        this.props.history.goBack();
    }

    render() {
        return (
            <div>
                 { this.props.ings ? 
                    <Aux>
                        {this.props.purchased && <Redirect to="/" />}
                        <CheckoutSummary 
                            checkoutCancelled={this.checkoutCancelledHandler}
                            checkoutContinued={this.checkoutContinuedHandler}
                            ingredients={this.props.ings}/>
                        <Route 
                            path={this.props.match.path + '/contact-data'} 
                            component={ContactData}/>
                    </Aux>
                    :
                    <Redirect to="/"/>
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        purchased: state.order.purchased
    }
}

export default connect(mapStateToProps)(Checkout)