import React, {Component} from 'react'
import { connect } from 'react-redux'
import Aux from '../../hoc/Auxiliary/Auxiliary'
import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/Build_Controls/BuildControls'
import Modal from '../../components/UI/Modal/Modal'
import OrderSummary from '../../components/Burger/Order_Summary/OrderSummary'
import Spinner from '../../components/UI/Spinner/Spinner'
import witherrorHandler from '../../hoc/withErrorHandler/withErrorHandler'
import * as actions from '../../store/actions/index';
import axios from '../../axios'

// Analisis : Karena Component ini ada yang di update (Modal), dan di dalam modal tersebut kan ada Modal, nah disitu kita kasih hooks shouldUpdated, biar lebih ringan / Cuma update yang perlu aja
// Contoh : Ketika kita tekan more or less.. dia di render, meskipun di browser kita hidden, nah seharusnya jika modalnya dihidden itu gk perlu di render
// purchaseContinueHandler: Harus pakai .json di urlnya untuk memasukkan data ke mongoDB atau Firebase, Orders.json itu table
// Output di encodeURIComponent : ntar bakal gini = ['salad=4', 'meat=2']
// Jika encodeuri tadi di join dengan & maka jadinya bakal : salad=4&meat=2 (Kaya GET di php lah), nah yang queryParams.push(encodeURIComponent(ig) : Keluarnya itu key contoh : bacon, meat, totalPrice, dll
// Lalu encodeURIComponent(this.state.ingredients[ig] itu value dari statenya, misal meat itu valuenya 2 
class BurgerBuilder extends Component {
    state = {
        purchasing: false,
        loadingModal: false,
        building: false,
    }

    componentDidMount() {
        this.props.onInitIngredients()
    }

    purchaseHandler = () => {
        if (this.props.isAuth) {
            this.setState({ purchasing: true })
        } else {
            this.props.onSetRedirectPath('/checkout')
            this.props.history.push('/auth')
        }
    }

    purchaseCancelHandler = () => {
        this.setState({ purchasing: false })
    }

    purchaseContinueHandler = () => {
        this.props.onInitPurchase()
        this.props.history.push('/checkout')
    }

    // Dikasih param biar dapat State yang updated
    // Map : Mencari Ingredient by igKey
    // Reduce : Dimulai dari angka 0 setelah itu jika ada datanya maka ditambah el
    // SetState : jika Sum > 0 maka true, jika tidak maka false
    // Reduce((sum, el)) => custom aja param nya, Sum nya ambil yang terakhir lalu ditambah el
    updatePurchaseState(ingredients) {
        const sum = Object.keys(ingredients)
            .map(igKey => {
                return ingredients[igKey]
            })
            .reduce((sum, el) => {
                return sum + el
            }, 0)

        return sum > 0;
    }

    render() {
        const disabledInfo = {
            ...this.props.ings
        }

        // ini dia filter, disabled[key] nya <= 0 maka disabled[key] jadi true, jika tidak maka false
        // {salad: true, bacon: false} 
        for (let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }

        // let orderSummary = null;
        // let Burger = this.state.error ? <p>Ingredients can't be loaded!</p> : <Spinner />;

        const { loadingModal, loading } = this.state;
        const { ings, onIngredientAdded, onIngredientRemoved, price, error } = this.props
        return (
            <Aux>
                <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
                    {
                        loadingModal || !ings
                        ? <Spinner/>
                        : <Aux>
                            <OrderSummary
                            ingredients={ings}
                            price={price}
                            purchaseCanceled={this.purchaseCancelHandler}
                            purchaseContinued={this.purchaseContinueHandler} />
                        </Aux>
                    }
                </Modal>
                {
                    error ? <p>Ingredients Can't be loaded!</p> :
                    !ings ? <Spinner /> :
                        <Aux>
                            <Burger ingredients={ings} />
                            <BuildControls
                            isAuth={this.props.isAuth}
                            ingredientAdded={onIngredientAdded}
                            ingredientRemoved={onIngredientRemoved}
                            disabled={disabledInfo}
                            purchasable={this.updatePurchaseState(this.props.ings)}
                            ordered={this.purchaseHandler}
                            price={price} />
                        </Aux>
                }
            </Aux>
        )
    }
}

const mapStateToProps = state => {
    return {
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        error: state.burgerBuilder.error,
        isAuth: state.auth.token !== null
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onIngredientAdded: (ingName) => dispatch( actions.addIng(ingName) ),
        onIngredientRemoved: (ingName) => dispatch( actions.rmvIng(ingName) ),
        onInitIngredients: () => dispatch( actions.initIngredients() ),
        onInitPurchase: () => dispatch( actions.purchaseInit() ),
        onSetRedirectPath: (path) => dispatch( actions.setAuthRedirectPath(path) )
    }
}

// Di witherrorHandler ada 2 Param, buat ngambil Component dan configure axiosnya
export default connect(mapStateToProps, mapDispatchToProps)(witherrorHandler(BurgerBuilder, axios));