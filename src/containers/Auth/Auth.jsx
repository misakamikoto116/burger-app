import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import Input from '../../components/UI/Input/Input'
import Button from '../../components/UI/Button/Button'
import Class from './Auth.module.css'
import * as actions from '../../store/actions/index'
import Spinner from '../../components/UI/Spinner/Spinner'
import { updateObject } from '../../shared/utility'
import { checkValidity } from '../../shared/validation'

// error.message didapat dari firebase (default / langsung jadi)
// checkValidity() dari validation.js, Bisa juga digabung ke utility
class Auth extends Component {
    state = {
        controls: {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Your Email'
                },
                label: 'Email',
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Your Password'
                },
                label: 'Password',
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false,
                touched: false
            },
        },
        isSignUp: true
    }

    componentDidMount() {
        if (!this.props.buildBurger && this.props.authRedirectPath !== '/') {
            this.props.onSetRedirectPath()
        }
    }

    inputChangedHandler = (e, controlName) => {
        const updatedControls = updateObject(this.state.controls, {
            [controlName]: updateObject(this.state.controls[controlName], {
                value: e.target.value,
                valid: checkValidity(e.target.value, this.state.controls[controlName].validation),
                touched: true
            })
        })

        this.setState({controls: updatedControls})
    }

    submitHandler = (e) => {
        e.preventDefault();
        const email = this.state.controls.email.value;
        const password = this.state.controls.password.value;
        this.props.onAuth(email, password, this.state.isSignUp)
    }

    swithAuthModeHandler = () => {
        this.setState(prevState => {
            return {isSignUp: !prevState.isSignUp}
        })
    }


    render() {
        const { controls, isSignUp } = this.state;
        const { loading, error, isAuth } = this.props;

        const formElementsArray = [];
        for (let key in controls) {
            formElementsArray.push({
                id: key,
                config: controls[key]
            })
        }

        const form = formElementsArray.map(formElement => (
            <Input 
                key={formElement.id}
                elementType={formElement.config.elementType} 
                elementConfig={formElement.config.elementConfig} 
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                label={formElement.config.label}
                validation={formElement.config.validation}
                changed={(event) => this.inputChangedHandler(event, formElement.id)}/>
        ))
        
        let authRedirect = null
        if (this.props.isAuth) {
            authRedirect = <Redirect to={this.props.authRedirectPath} />
        }

        return (
            <div className={Class.Auth}>
                {authRedirect}
                { error && <p>{error.message}</p> }
                <form onSubmit={this.submitHandler}>
                    {!loading ? form : <Spinner />}
                    <Button btnType="Success">SUBMIT</Button>
                </form>
                <Button
                    clicked={this.swithAuthModeHandler} 
                    btnType="Danger">Switch To {isSignUp ? 'SIGNIN' : 'SIGNUP'}</Button>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuth: state.auth.token !== null,
        buildBurger: state.burgerBuilder.building,
        authRedirectPath: state.auth.authRedirectPath
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (email, password, isSignUp) => dispatch( actions.auth(email, password, isSignUp) ),
        onSetRedirectPath: () => dispatch( actions.setAuthRedirectPath('/') )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);