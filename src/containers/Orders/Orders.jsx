import React, {Component} from 'react'
import Class from './Orders.module.css'
import { connect } from 'react-redux'
import Order from '../../components/Burger/Order/Order'
import Axios from '../../axios';
import * as actions from '../../store/actions/index'
import Spinner from '../../components/UI/Spinner/Spinner'
import withHandlerError from '../../hoc/withErrorHandler/withErrorHandler'

// FetchOrder : Menambahkan Key ID di semua data order.. Karena dari awal gk dibuat Column ID nya
// +order.price : convert ke INT
class Orders extends Component {
    componentDidMount() {
       this.props.fetchOrders(this.props.token, this.props.userId)
    }
    
    render() {
        return (
             <div className={Class.Orders}>
                 {this.props.loading ? <Spinner /> :
                    this.props.orders.map(order => (
                        <Order 
                            key={order.id}
                            ingredients={order.ingredients}
                            price={+order.price}/>
                    ))
                 }
             </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        orders: state.order.orders,
        loading: state.order.loading,
        token: state.auth.token,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchOrders : (token, userId) => dispatch(actions.fetchOrders(token, userId)) 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withHandlerError(Orders, Axios))