// Firebase url
import axios from 'axios'

const instance = axios.create({
    baseURL: "https://my-buerger.firebaseio.com/",
})

export default instance;